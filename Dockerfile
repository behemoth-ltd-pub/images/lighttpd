FROM debian:stable
MAINTAINER admin@itk98.net

RUN apt update && apt dist-upgrade

RUN apt install -y lighttpd lighttpd-mod-deflate
RUN apt clean all

EXPOSE 80:80/tcp
EXPOSE 443:443/tcp
STOPSIGNAL SIGTERM

ENTRYPOINT ["/usr/sbin/lighttpd"]
CMD ["-D", "-f", "/etc/lighttpd/lighttpd.conf"]
